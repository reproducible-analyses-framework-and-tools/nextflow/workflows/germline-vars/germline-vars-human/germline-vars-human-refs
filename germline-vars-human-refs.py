import subprocess
import os

manual = []

# Set path to current directory
path = os.getcwd()
sums_path = path + '/human_sums.txt'


def check_prior(file):
	prior = str(subprocess.check_output([f'find . -name {file}'], shell=True))
	if file in prior:
		return True
	else:
		return False


def reference_check(i):
	with open(sums_path, 'r') as f:
		if i in f.read():
			print('File successfully validated: ' + str(i).rsplit(' ', 1)[1])
			return True
		else:
			print('File failed validation: ' + str(i).rsplit(' ', 1)[1])
			return False


def download_reference(cmd, alt=None):
	if alt == None:
		file = cmd.rsplit('/', 1)[1]
	else:
		file = alt
	attempt = 0
#   while True:
#		if attempt == 3:
#			manual.append(f'{file}')
#			return False
	subprocess.run([f'{cmd}'], shell=True)
#		check = str(subprocess.check_output([f'md5sum {file}'], shell=True)).split("'", 1)[1].rsplit("\\", 1)[0]
#		if reference_check(check):
	return True
#		else:
#			subprocess.run([f'rm {file}'], shell=True)
#			attempt += 1


### Genomic reference ###

subprocess.run([f'mkdir -p {path}/references/homo_sapiens'], shell=True)
os.chdir(f'{path}/references/homo_sapiens')

if not check_prior('Homo_sapiens.assembly38.no_ebv.fa'):
	os.chdir(f'{path}/references/homo_sapiens')
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/fasta'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/fasta')

	if download_reference('wget https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.fasta'):
		subprocess.run(['singularity pull docker://staphb/samtools:1.13'], shell=True)
		subprocess.run(['singularity exec -B $PWD samtools*.s* samtools faidx Homo_sapiens_assembly38.fasta'], shell=True)
		subprocess.run(["awk '{print $1}' Homo_sapiens_assembly38.fasta.fai | grep -v chrEBV > keep_ids"], shell=True)
		subprocess.run(["singularity exec -B $PWD samtools*.s* samtools faidx -r keep_ids -o Homo_sapiens.assembly38.no_ebv.fa Homo_sapiens_assembly38.fasta"],
					   shell=True)
		subprocess.run(['rm Homo_sapiens_assembly38.fasta'], shell=True)
		subprocess.run(['rm Homo_sapiens_assembly38.fasta.fai'], shell=True)
		subprocess.run(['rm samtools*.s*'], shell=True)
		subprocess.run(['rm keep_ids'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: Homo_sapiens.assembly38.no_ebv.fa')


### GTF/GFF3 ###

if not check_prior('gencode.v37.annotation.with.hervs.gtf'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/annot'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/annot')

	if download_reference('wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_37/gencode.v37.annotation.gtf.gz') and \
		download_reference('wget http://geve.med.u-tokai.ac.jp/download_data/gtf_m/Hsap38.geve.m_v1.gtf.bz2'):
		subprocess.run(['bzip2 -d Hsap38.geve.m_v1.gtf.bz2'], shell=True)
		subprocess.run(['zcat gencode.v37.annotation.gtf.gz > gencode.v37.annotation.with.hervs.gtf;'], shell=True)
		subprocess.run(["cat Hsap38.geve.m_v1.gtf | sed 's/^/chr/g' | sed 's/CDS/transcript/g' >> gencode.v37.annotation.with.hervs.gtf"], shell=True)
		subprocess.run(["cat Hsap38.geve.m_v1.gtf | sed 's/^/chr/g' | sed 's/CDS/exon/g' >> gencode.v37.annotation.with.hervs.gtf"], shell=True)
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: gencode.v37.annotation.with.hervs.gtf')


if not check_prior('gencode.v37.annotation.gff3'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/annot'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/annot')
	if download_reference('wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_37/gencode.v37.annotation.gff3.gz'):
		subprocess.run(['gunzip gencode.v37.annotation.gff3.gz'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: gencode.v37.annotation.gff3.gz')


### VCF references ###

if not check_prior('1000g_pon.hg38.vcf.gz'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/vcfs'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/vcfs')
	download_reference('wget https://storage.googleapis.com/gatk-best-practices/somatic-hg38/1000g_pon.hg38.vcf.gz')
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: 1000g_pon.hg38.vcf.gz')

if not check_prior('af-only-gnomad.hg38.vcf.gz'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/vcfs'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/vcfs')
	download_reference('wget https://storage.googleapis.com/gatk-best-practices/somatic-hg38/af-only-gnomad.hg38.vcf.gz')
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: af-only-gnomad.hg38.vcf')

if not check_prior('Homo_sapiens_assembly38.dbsnp138.vcf.gz'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/vcfs'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/vcfs')
	download_reference('wget https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.dbsnp138.vcf')
	subprocess.run(['bgzip Homo_sapiens_assembly38.dbsnp138.vcf'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: Homo_sapiens_assembly38.dbsnp138.vcf.gz')

if not check_prior('small_exac_common_3.hg38.vcf.gz'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/vcfs'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/vcfs')
	download_reference('wget https://storage.googleapis.com/gatk-best-practices/somatic-hg38/small_exac_common_3.hg38.vcf.gz')
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: small_exac_common_3.hg38.vcf.gz')


### BEDs ###
 
if not check_prior('hg38_exome.bed'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/beds'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/beds')
	subprocess.run([f'wget https://gitlab.com/landscape-of-effective-neoantigens-software/nextflow/modules/tools/lens/-/wikis/uploads/ea7d0b56f0af067ea9e7a3c0e9c96c96/hg38_bed_generator.sh'], shell=True)
#	attempt = 0
#	while True:
	subprocess.run(['bash hg38_bed_generator.sh ../annot/gencode.v37.annotation.gtf.gz'], shell=True)
#		check = str(subprocess.check_output([f'md5sum hg38_exome.bed'], shell=True)).split("'", 1)[1].rsplit("\\", 1)[0]
#		if attempt == 3:
#			manual.append(f'hg38_exome.bed')
#			break
#		if reference_check(check):
#			break
#		else:
#			subprocess.run([f'rm hg38_exome.bed'], shell=True)
#			attempt += 1

	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: hg38_exome.bed')


### Show list of files that could not be validated ###

if len(manual) > 0:
	print('These files could not be validated and must be downloaded manually. Please refer to the LENS documentation.')
	for item in manual:
		print(item)
else:
	print('All human references downloaded successfully!')
